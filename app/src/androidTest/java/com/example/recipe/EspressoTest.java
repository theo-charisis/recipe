package com.example.recipe;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.SmallTest;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by ΤΕΟ on 15/12/2015.
 */
/////   CHECKS NAME OF THE TEXTVIEW AND DETAILS,IT INSERTS RANDOM TAXT IN THESE AREAS/////

@RunWith(AndroidJUnit4.class)
@SmallTest
public class EspressoTest {

    @Rule
    public ActivityTestRule<DetailPage> activityTestRule = new ActivityTestRule<>(
            DetailPage.class);
    @Test
    public void validateEditText() {
        onView(withId(R.id.editName)).perform(typeText("Hello This is the name section"), closeSoftKeyboard()).check(matches(withText("Hello This is the name section")));

        onView(withId(R.id.editDetail)).perform(typeText("This is detail Page"), closeSoftKeyboard()).check(matches(withText("This is detail Page")));
        onView(withId(R.id.buttonLoadPicture)).perform(click());

    }
}
