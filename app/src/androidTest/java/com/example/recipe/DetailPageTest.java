package com.example.recipe;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.SmallTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by ΤΕΟ on 15/12/2015.
 */

///CHECKS GRIDVIEW PAGE AND BACK AGAIN TO PHOTOLIST PAGE IF WORKING////

@RunWith(AndroidJUnit4.class)
@SmallTest
public class DetailPageTest {
    @Rule
    public ActivityTestRule<PhotoListActivity> activityTestRule = new ActivityTestRule<>(
            PhotoListActivity.class);

    @Test
    public void changeText_PhotoListActivity() {
        onView(withId(R.id.action_grid)).perform(click()).check(matches(isDisplayed()));
        onView(withId(R.id.action_photo_list)).perform(click()).check(matches(isDisplayed()));


    }
}
