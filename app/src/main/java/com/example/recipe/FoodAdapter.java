package com.example.recipe;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by ΤΕΟ on 27/10/2015.
 */

public class FoodAdapter extends ArrayAdapter<Food> {

    private int resource;
    private ArrayList<Food> Food;
    private Context context;

    public FoodAdapter(Context context, int resource, ArrayList<Food> Food) {
        super(context, resource, Food);
        this.resource = resource;
        this.Food = Food;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View v = convertView;
        try{
            if (v == null){
                LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = layoutInflater.inflate(resource, parent, false);
            }

            ImageView imageView = (ImageView) v.findViewById(R.id.imageView);
            TextView textViewName = (TextView) v.findViewById(R.id.textViewName);
            TextView textViewDetail = (TextView) v.findViewById(R.id.textViewDetail);

            imageView.setImageResource(Food.get(position).getPhoto());
            textViewName.setText(Food.get(position).getFoodName());
            textViewDetail.setText(Food.get(position).getDetail());

        }
        catch (Exception e){
            e.printStackTrace();
            e.getCause();
        }
        return v;
    }

}