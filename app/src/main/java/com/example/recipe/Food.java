package com.example.recipe;

/**
 * Created by ΤΕΟ on 27/10/2015.
 */
public class Food {
   // int id;
    public String FoodName;
    public String Detail;
    public int Photo;

   public Food(  String name, String detail){
       this.FoodName = name;
       this.Detail = detail;
   }

   public Food( String name, String detail, int photo) {

       //this.id = id;
        this.FoodName = name;
        this.Detail = detail;
        this.Photo = photo;
    }

     // getting ID
   // public int getID(){
   //     return this.id;
   // }

    // setting id
    //public void setID(int id){
   //     this.id = id;
    //}

    public String getFoodName() {
        return this.FoodName;
    }
    public void setFoodName(String name) {
        this.FoodName = name;
    }

    public String getDetail() {
        return this.Detail;
    }
    public void setDetail(String detail) {
        this.Detail = detail ;
    }

    public int getPhoto() {
        return this.Photo;
    }

   // @Override
  //  public String toString() {
  //      return Detail;
  //  }
}
