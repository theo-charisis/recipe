package com.example.recipe;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by ΤΕΟ on 14/11/2015.
 */
public class FoodDataSource {
    private SQLiteDatabase database;
    private DatabaseHandler dbHelper;

    public FoodDataSource(Context context) {
        dbHelper = new DatabaseHandler(context);

    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }
    public boolean insertFood (Food c){
        boolean didSucceed = false;
        try {
            ContentValues initialValues = new ContentValues();

            initialValues.put("name", c.getFoodName());
            initialValues.put("detail", c.getDetail());
            initialValues.put("photo", c.getPhoto());

            didSucceed = database.insert("Food",null,initialValues) > 0;
        }
        catch (Exception e){
            //Do nothing
        }
        return  didSucceed;
    }
    public boolean updateFood (Food c) {
        boolean didSucceed = false;
        try {
            Long rowID = Long.valueOf(c.getFoodName());
            ContentValues updateValues = new ContentValues();

            updateValues.put("name", c.getFoodName());
            updateValues.put("detail", c.getDetail());
            updateValues.put("photo", c.getPhoto());

            didSucceed = database.update("Food", updateValues, "_id=" + rowID, null) >0;
        }
        catch (Exception e){
            //Do nothing
        }
        return  didSucceed;
    }
    public boolean deleteFood(int FoodID) {
        boolean didDelete = false;
        try {
            didDelete = database.delete("Food","_id=" + FoodID, null) > 0;
        }
        catch (Exception e){
            //DO nothing
        }
        return didDelete;
    }


    public ArrayList<String> getFoodName() {
        ArrayList<String> foodNames = new ArrayList<String>();
        try {
            String query = "Select food from list";
            Cursor cursor = database.rawQuery(query, null);

            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                foodNames.add(cursor.getString(0));
                cursor.moveToNext();
            }
            cursor.close();
        } catch (Exception e) {
            foodNames = new ArrayList<String>();
        }
        return foodNames;
    }
}