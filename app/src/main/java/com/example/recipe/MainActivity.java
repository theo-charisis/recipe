package com.example.recipe;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.EditText;
import android.widget.ToggleButton;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {
    private ListView listView;
    private String[] foodNames;
    private EditText DetailText;
    private EditText nameText;
    private Food currentFood;
    // currentFood = new Food();


    @Override

    protected void onCreate(Bundle savedInstanceState) {
        startActivity(new Intent(this, PhotoListActivity.class));
        
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        foodNames = getResources().getStringArray(R.array.foodNames);
        // nameText = (EditText) findViewById(R.id.nameText);
        //DetailText = (EditText) findViewById(R.id.DetailText);


        listView = (ListView) findViewById(R.id.listView);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, foodNames);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(

                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                        Intent intent = new Intent(MainActivity.this, PhotoListActivity.class);
                        startActivity(intent);
                    }
                }
        );

    }

    private void initTextChangedEvents() {
        final EditText FoodName = (EditText) findViewById(R.id.editName);
        FoodName.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                currentFood.setFoodName(FoodName.getText().toString());
            }

            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                //Auto-generated method stub
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //Auto-generated method stub
            }
        });
        final EditText Detail = (EditText) findViewById(R.id.editDetail);
        Detail.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                currentFood.setDetail(Detail.getText().toString());
            }

            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                //Auto-generated method
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //Auto-generated method
            }

        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_photo_list:
                startActivity(new Intent(this, PhotoListActivity.class));
                return true;
            case R.id.action_grid:
                startActivity(new Intent(this, GridActivity.class));
                return true;
            //case R.id.action_add:
            //startActivity(new Intent(this, addActivity.class));
            case R.id.action_add:
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }


}

