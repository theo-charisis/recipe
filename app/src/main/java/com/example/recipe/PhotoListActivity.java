package com.example.recipe;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class PhotoListActivity extends AppCompatActivity {

    public static int[] foodPhotos = {
            R.drawable.chickenparmigiana,
            R.drawable.crepe,
            R.drawable.lasagna,
            R.drawable.lemonrice,
            R.drawable.mousakas,
            R.drawable.carbonara,
            R.drawable.pizza,
            R.drawable.salad,
            R.drawable.chow
    };

    private String[] foodNames;
    private String[] foodDetails;
    private ArrayList<Food> food = new ArrayList<>();


    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_list);
      /* */  DatabaseHandler database = new DatabaseHandler(this);
        

        /* */

        foodNames = getResources().getStringArray(R.array.foodNames);
        foodDetails = getResources().getStringArray(R.array.foodDetails);
         generatefood();


        listView = (ListView) findViewById(R.id.listView2);
        listView.setAdapter(new FoodAdapter(this, R.layout.list_item, food));
        listView.setOnItemClickListener(

                new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        Intent intent = new Intent(PhotoListActivity.this, DetailPage.class);
                        intent.putExtra("POSITION", position);
                        startActivity(intent);

                    }
                }
        );

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        switch (id){
            case R.id.action_photo_list:
                startActivity(new Intent(this, PhotoListActivity.class));
                return true;
            case R.id.action_grid:
                startActivity(new Intent(this, GridActivity.class));
                return true;
            //case R.id.action_add:
            //startActivity(new Intent(this, addActivity.class));
            case R.id.action_add:
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    private void generatefood() {

        for (int i = 0; i < foodPhotos.length; i++) {
           food.add(new Food(foodNames[i], foodDetails[i], foodPhotos[i]));
        }
   }

}