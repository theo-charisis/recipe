package com.example.recipe;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;


public class GridActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view);
        GridView gridview = (GridView) findViewById(R.id.gridView);
        gridview.setAdapter(new ImageAdapter(this));

         gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
             public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                 //Toast.makeText(getBaseContext(), "At position " + position + " is " + getResources().getStringArray(R.array.foodNames)[position], Toast.LENGTH_SHORT).show();
             }
         });
        gridview.setOnItemClickListener(


        new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                // Toast.makeText(getBaseContext(), "You clicked " + Food.get(position), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(GridActivity.this, DetailPage.class);
                intent.putExtra("POSITION", position);
                startActivity(intent);

            }
        }
            );
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        switch (id){
            case R.id.action_photo_list:
                startActivity(new Intent(this, PhotoListActivity.class));
                return true;
            case R.id.action_grid:
                startActivity(new Intent(this, GridActivity.class));
                return true;
            //case R.id.action_add:
            //startActivity(new Intent(this, addActivity.class));
            case R.id.action_add:
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }
}