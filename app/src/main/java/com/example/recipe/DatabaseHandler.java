package com.example.recipe;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ΤΕΟ on 10/11/2015.
 */
public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 4;

    private static final String DATABASE_NAME = "DetailPage.db";
    private static final String TABLE_NAME = "FOOD";
    private static final String COLUMN_ID = "id";
    private static final String TABLE_FOOD = "food";

    private static final String KEY_ID = "id";
    private static final String KEY_FOODNAME = "name";
    private static final String KEY_DETAIL = "detail";
    private static final String KEY_PHOTO = "photo";




    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        String CREATE_TABLE_FOOD = "CREATE TABLE " + TABLE_FOOD + "("
                + KEY_ID + " INTEGER PRIMARY KEY, "
                + KEY_FOODNAME + " TEXT, "
                + KEY_DETAIL + " TEXT " + ")";
        //      + KEY_PHOTO +
        database.execSQL(CREATE_TABLE_FOOD);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(DatabaseHandler.class.getName(),
                "Upgrading database from version" + oldVersion + "to"
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS FOOD");
        onCreate(db);


    }

    void addFood(Food food) {
        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_FOODNAME, food.getFoodName());
        values.put(KEY_DETAIL, food.getDetail());
        //values.put("KEY_PHOTO", food.getPhoto());

        database.insert(TABLE_FOOD, null, values);

        //  long result = db.insert("foodTable", null, values);
        //  if (result > 0) {
        Log.d("DatabaseHandler", "inserted successfully");
        //// } else {
        //     Log.d("DatabaseHandler", "failed to insert");
        //  }
        database.close();
    }



    // Getting single food

    // Getting All foods
    public void getAllfood() {
        List<Food> foodList = new ArrayList<Food>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_FOOD;

        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
    }
}

     // Updating single food
  //  public int updateFood(Food food) {
     //   SQLiteDatabase database = this.getWritableDatabase();

      //  ContentValues values = new ContentValues();
      //  values.put(KEY_FOODNAME, food.getFoodName());
      //  values.put(KEY_DETAIL, food.getDetail());

        // updating row
       // return database.update(TABLE_FOOD, values, KEY_ID + " = ?",
           //     new String[] { String.valueOf(food.getID()) });
   // }


//}